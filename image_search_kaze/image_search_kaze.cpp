// hBOW_KAZE.cpp : Defines the entry point for the console application.
//


#include "stdafx.h"

#include "image_search_KAZE.h"

// it is important to not add features computed for each image in a single vector and then generate a vocabulary
// KAZE features are too heavy to be stored in a single vector
// instead try adding KAZE features one by one to the vocabulary or the database produced from the vocabulary



// ----------------------------------------------------------------------------
int main()
{
	// Get file names for vocabulary and database
	vocDbNames();

	cout << "Using KAZE algorithm ... " << endl;
	if(!is_file_exist(db_str.c_str()))
	{
		// find image file names
		cout << "reading image filenames..." << endl;
		readFileNames(dir_path);

		if(img_fnames.empty())
		{
			cout << "no image files found" << endl;
			return -1;
		}

		cout << "No. of images found: " << img_fnames.size() << endl;

		//compute KAZE Features
		kazeFeatures(kaze_features);
		
		// create vocabulary & database
		kazeVocDbCreation(kaze_features);	
	}	
	else
	{
		if(!input_from_webcam)
			loadAndSearchKaze(db_str.c_str());
		else
			loadFromWebcamAndSearchKaze(db_str.c_str());
	}


	return 1;
}


// ----------------------------------------------------------------------------

void kazeQueryFeatures(vector<vector<vector<float> > > &features)
{
	features.clear();
	features.reserve(1);

	cv::Mat query_image = cv::imread("pill-test-r60.png",0);

	if(!query_image.empty())
	{
		cv::Mat image_32;

		cv::Mat desc;
		vector<cv::KeyPoint> keypoints;
		vector<float> descriptors;

		// KAZE non-opencv

		KAZEOptions options;
		// Convert the image to float
		query_image.convertTo(image_32,CV_32F,1.0/255.0,0);

		options.img_width = query_image.cols;
		options.img_height = query_image.rows;

		// Create the KAZE object
		KAZE evolution(options);

		// Create the nonlinear scale space
		evolution.Create_Nonlinear_Scale_Space(image_32);

		evolution.Feature_Detection(keypoints);
		evolution.Compute_Descriptors(keypoints,desc);


		/*
		// KAZE opencv3
		cv::Ptr<cv::KAZE> kaze = cv::KAZE::create();

		kaze->detectAndCompute(query_image, cv::noArray(), keypoints, desc);
		*/

		cout << "Size of keypoints: " << keypoints.size() << endl;
		cout << "Size of descriptor matrix: " << desc.rows << ", " << desc.cols << endl;

		descriptors.assign((float*)desc.datastart, (float*)desc.dataend);

		cout << "changing structure of descriptor vectors" << endl;

		features.push_back(vector<vector<float> >());
		changeStructure(descriptors, features.back(), desc.cols);
	}
	else
		cout << "query.jpg not found" << endl;

}

// ----------------------------------------------------------------------------

void kazeFeatures(vector<vector<vector<float> > > &features)
{
  features.clear();
  //features.reserve(NIMAGES);
  
  cv::FileStorage fs(feat_str, cv::FileStorage::WRITE);

  cout << "Extracting KAZE features... " << endl;
  for(int i = 0; i < NIMAGES; ++i)
  {
    cv::Mat image = cv::imread(img_fnames[i], 0);

	cout << "Processing for image- " << i << ": " << img_fnames[i] << endl;
		
	if(image.empty())			
		continue;

	cv::Mat image_32;

	if(image.empty())
	{
		cout << img_fnames[i] << ".jpg not found" << endl;
		continue;
	}

    cv::Mat desc;
    vector<cv::KeyPoint> keypoints;
    vector<float> descriptors;
	
	// non-opencv 3 KAZE
	KAZEOptions options;
    // Convert the image to float
	image.convertTo(image_32,CV_32F,1.0/255.0,0);

	options.img_width = image.cols;
	options.img_height = image.rows;

	// Create the KAZE object
	KAZE evolution(options);

	// Create the nonlinear scale space
	evolution.Allocate_Memory_Evolution();
	cout << "memory allocated" << endl;
	evolution.Create_Nonlinear_Scale_Space(image_32);
	cout << "non linear scale space created" << endl;
	evolution.Feature_Detection(keypoints);
	evolution.Compute_Descriptors(keypoints,desc);

	/*
	// OpenCV 3 KAZE
	cv::Ptr<cv::KAZE> kaze = cv::KAZE::create();

	kaze->detectAndCompute(image, cv::noArray(), keypoints, desc);
	*/
	cout << "Size of keypoints: " << keypoints.size() << endl;
	cout << "Size of descriptor matrix: " << desc.rows << ", " << desc.cols << endl;
	keypoints.clear();
	
	descriptors.assign((float*)desc.datastart, (float*)desc.dataend);

	//write features to file
	stringstream ss;
	ss << NIMAGES;
	string str = ss.str();
	//write(fs, str, desc);

	/*
	for(size_t r = 0; r < desc.rows; r++)
		//for(size_t c = 0; c < desc.cols; c++)
		{
			//cout << "c,r: " << c << ", " << r << endl;
			descriptors.push_back(desc.ptr<float>(r));
		}
		*/
	cout << "changing structure of descriptor vectors" << endl;
	//
    features.push_back(vector<vector<float> >());
    changeStructure(descriptors, features.back(), desc.cols);
	desc.release();
	descriptors.clear();
  }

  fs.release();

}

// ----------------------------------------------------------------------------

void changeStructure(const vector<float> &plain, vector<vector<float> > &out,
  int L)
{
  out.resize(plain.size() / L);
  cout << "size of descriptor vectors:" << plain.size() << endl;
  unsigned int j = 0;
  for(unsigned int i = 0; i < plain.size(); i += L, ++j)
  {
    out[j].resize(L);
    std::copy(plain.begin() + i, plain.begin() + i + L, out[j].begin());
  }
  cout << "done!" << endl;
}


// ----------------------------------------------------------------------------

void kazeVocDbCreation(const vector<vector<vector<float> > > &features)
{
  Surf64Vocabulary voc(k, L, weight, score);

  if(!is_file_exist(voc_str.c_str()))
	{
		cout << "Creating a " << k << "^" << L << " vocabulary..." << endl;
		voc.create(features);
		cout << "... done!" << endl;

		cout << "Vocabulary information: " << endl << voc << endl << endl;

		voc.save(voc_str);
	}
	else
		// load the vocabulary from disk
		Surf64Vocabulary voc(voc_str);
	
	// vocabulary db
	Surf64Database db(voc, false, 0);
  
	// add image features to the database
	cout << "\nAdding image features to database .. " << "\n" << endl;

	for(int i = 0; i < NIMAGES; i++)
	{
		cout << "adding for image: " << img_fnames[i] << endl;
		db.add(features[i]);
	}

	// we can save the database. The created file includes the vocabulary
	// and the entries added
	cout << "Saving database..." << endl;
	db.save(db_str);
	cout << "... done!" << endl;
}


// ----------------------------------------------------------------------------

void loadAndSearchKaze(const char* dbname)
{	
	readFileNames(dir_path);
	// features from query image
	vector<vector<vector<float> > > features_query;
	kazeQueryFeatures(features_query);
  
	if(features_query.empty())
		cout << "No features found for query image" << endl;
	else
	{
		cout << "No of features from query image: " << features_query[0].size() << "\n\n";
		cout << "Search in vocabulary for match against query image" << "\n\n";
		
		clock_t start = clock();

		Surf64Database db(dbname);

		clock_t end = clock();		

		cout << "Loaded database: " << db << endl << endl;

		cout << "Time taken to load database: " <<  (float)(end - start) / CLOCKS_PER_SEC << endl << endl;

		//db.add(features_query[0]);

		QueryResults ret;
		db.query(features_query[0], ret, 4);
		
		// ret[0] is always the same image in this case, because we added it to the 
		// database. ret[1] is the second best match.
		if(!ret.empty())
		{
			cout << "Searching for Query Image. " << ret << endl;
			
			clock_t end_search = clock();

			cout << "Time taken to search for query: " <<  (float)(end_search - end) / CLOCKS_PER_SEC << endl << endl;

			stringstream result_img_str;
			result_img_str << img_fnames[ret[0].Id].c_str();

			cout << "Name of most matching image: " << img_fnames[ret[0].Id] << endl;

			cv::Mat result_img = cv::imread(result_img_str.str());

			cv::imshow("Query Result", result_img);
			cv::waitKey(0);
		}
		else
			cout << "No results found for the query" << endl;
		cout << endl;
	}

}


// ----------------------------------------------------------------------------


int loadFromWebcamAndSearchKaze(const char* dbname)
{	
	// Read filenames
	readFileNames(dir_path);
	
	if(img_fnames.empty())
	{
		cout << "No reference images found ... " << endl;
		return -1;
	}

	cout << "Performing webcam based testing ... " << endl;

	// load database
	Surf64Database db(dbname);

	cout << "Loaded database: " << db << endl;

	// features from query image
	vector<vector<vector<float> > > features_query;
 
	KAZEOptions options;

	// capture video from webcam
	cv::VideoCapture cap(0);
	
	cv::Mat frame, img;
	cap >> frame;
	char k = ' ';
	cv::namedWindow("Result");
	while(!frame.empty() && k !='q')
	{
		cap >> img;
		//frame = img;
		cvtColor(img, frame, CV_BGR2GRAY);
		imshow("Query", frame);
		k = cv::waitKey(10);

		if(k =='s')
		{
			features_query.clear();
			features_query.reserve(1);

			cv::Mat desc;
			vector<cv::KeyPoint> keypoints;
			vector<float> descriptors;

			/*
			// OpenCV3 KAZE
			cv::Ptr<cv::KAZE> kaze = cv::KAZE::create();
			kaze->detectAndCompute(img, cv::noArray(), keypoints, desc);
			*/

			// Non-OpenCV KAZE
			
			// Convert the image to float
			cv::Mat image_32;
			frame.convertTo(image_32,CV_32F,1.0/255.0,0);
			cout << "size of image_32: " << image_32.size() << endl;
			options.img_width = frame.cols;
			options.img_height = frame.rows;

			// Create the KAZE object
			KAZE evolution(options);

			// Create the nonlinear scale space
			cout << "create non-linear scale space" << endl;
			evolution.Create_Nonlinear_Scale_Space(image_32);


			/*
			future<bool> fut = async(fut_wait_for, evolution, image_32);   
			chrono::milliseconds span (1000);

			while (fut.wait_for(span)==future_status::timeout)
				cout << '.';
			
			fut.get();
			*/

			//evolution.Create_Nonlinear_Scale_Space(image_32);
			cout << "non-linear scale space created " << endl;
			evolution.Feature_Detection(keypoints);
			evolution.Compute_Descriptors(keypoints,desc);
			
			if(desc.empty())
				cout << "No descriptors found for query image" << endl;
			else
			{
				cout << "Size of keypoints: " << keypoints.size() << endl;
				cout << "Size of descriptor matrix: " << desc.rows << ", " << desc.cols << endl;

				descriptors.assign((float*)desc.datastart, (float*)desc.dataend);

				cout << "changing structure of descriptor vectors" << endl;

				features_query.push_back(vector<vector<float> >());
				changeStructure(descriptors, features_query.back(), desc.cols);
				
				cout << "Querying database for captured frame ... " << endl;
				QueryResults ret;
				db.query(features_query[0], ret, 1);
				cout << "Results found: " << ret.size() << endl;
				// ret[0] is always the same image in this case, because we added it to the 
				// database. ret[1] is the second best match.

				if(!ret.empty())
				{
					cv::Mat result = cv::imread(img_fnames[ret[0].Id]);
					cv::imshow("Result", result);
					cout << "Searching for Query Image. " << ret << endl;
					cout << "Name of most matching image: " << ret[0].Id << endl;
				}
				else
				{
					cout << "No results found for the query" << endl;
					cout << endl;
				}
				
				cout << "... done!" << endl;
			}

			k = cv::waitKey(0);
		}
	}

	return 1;
}

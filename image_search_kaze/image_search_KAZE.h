#include <iostream>
#include <vector>
#include "dirent.h"
#include <ctime>

// DBoW2
#include "DBoW2.h" // defines Surf64Vocabulary and Surf64Database
#include "DUtils.h"
#include "DUtilsCV.h" // defines macros CVXX
#include "DVision.h"

// OpenCV
#include <opencv2/opencv.hpp>

// KAZE
#include <KAZE.h>

using namespace DBoW2;
using namespace DUtils;
using namespace DVision;
using namespace std;


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
// Global variables
// number of training images
int NIMAGES = 103;
const string dir_path = "C:\\Users\\abgg\\Downloads\\od\\image_search_Release\\PNG";//"C:\\Users\\abgg\\Downloads\\odesk\\Apparell_Recognition\\album_covers2"; //"C:\\Users\\abgg\\Downloads\\odesk\\Apparell_Recognition\\test_image_set\\training images";   //"C:\\Users\\abgg\\Downloads\\odesk\\Apparell_Recognition\\album_covers";
//const string dir_path = "C:\\Users\\abgg\\Downloads\\odesk\\Apparell_Recognition\\new2"; //"C:\\Users\\abgg\\Downloads\\odesk\\Apparell_Recognition\\test_image_set\\training images";   //
vector<string> img_fnames;
vector<vector<vector<float > > > kaze_features;
bool input_from_webcam = false;

// vocabulary params
const int k =  10;
const int L =  6;
const WeightingType weight = TF_IDF;
const ScoringType score = L1_NORM;

// file names
string voc_str, db_str, feat_str;

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 

void loadAndSearchKaze(const char* dbname);
int loadFromWebcamAndSearchKaze(const char* dbname);
void kazeFeatures(vector<vector<vector<float > > > &features);
void kazeQueryFeatures(vector<vector<vector<float > > > &features);
void kazeVocDbCreation(const vector<vector<vector<float > > > &features);
void changeStructure(const vector<float> &plain, vector<vector<float> > &out, int L);

// ----------------------------------------------------------------------------

void wait()
{
  cout << endl << "Press enter to continue" << endl;
  getchar();
}


// check if vocabulary file exist
bool is_file_exist(const char *fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
}


void vocDbNames()
{
	// vocabulary name
	stringstream ss_k, ss_L, ss_NIMAGES;
	ss_k << k;
	ss_L << L;
	ss_NIMAGES << NIMAGES;

	ss_k >> voc_str;
	voc_str.append("k_");
	voc_str.append(ss_L.str());
	voc_str.append("L_");
	voc_str.append(ss_NIMAGES.str());
	voc_str.append("NIMAGES_");

	db_str = voc_str;
	feat_str = voc_str;
	feat_str.append("kaze_feat.yml");
	voc_str.append("kaze_voc.yml.gz");
	db_str.append("kaze_db.yml.gz");
}

// ----------------------------------------------------------------------------

void readFileNames(string dir_str)
{		
	DIR *dir;
	struct dirent *ent;
	int im_count = 0, loop_count = 0;	
	if ((dir = opendir (dir_str.c_str())) != NULL) 
	{
		/* print all the files and directories within directory */
		while ((ent = readdir (dir)) != NULL)
		{
			if(loop_count < 2)
			{
				loop_count++;
				continue;
			}

			if(im_count > NIMAGES-1)
				break;

			stringstream ss;
			ss << im_count;
			string img_path = dir_path;
			img_path.append("\\");
			img_path.append(ent->d_name);
			cout << img_path << endl;
			//cv::Mat img = cv::imread(img_path);
			img_fnames.push_back(img_path);
			im_count++;
		}
		closedir (dir);
	}
}
